DEMO LINK: https://drive.google.com/file/d/15UJArZrwHRmuuiVZlcbjdAhtIp_Ixlsa/view?usp=sharing

User Manual 

I - Introduction

The wireless interface was implemented to allow users to operate The Game while not being tethered to the seat. Instead, feel free to stretch your legs while helping The Guy dodge brightly colored walls as he cruises through virtual reality. In only a few minutes, your system can be set up and powered on, allowing for free range gaming of the highest quality.


II - Set Up
	In order for one to enjoy the thrills of wireless gaming, there is a short materials list required. 
●	2 Basys 3 Boards
●	2 nRF24L01+  Transceivers Assembly (transceiver + voltage regulator)
●	VGA Capable Monitor
●	Assorted Wires and Cables
○	VGA Cable
○	2 Basys 3 power cables
○	Male - Female wires for PMOD connection

The set up for the wireless system begins with the assembly of the transceiver modules which merely consists of attaching the voltage regulator to the transceiver. Once these devices are connected, they will be wired to the Basys 3 boards through the use of the PMOD connections. Figure A is located above, and has several key components of the Basys board circled. The PMOD connection can be seen circled in black and marked with a black “A”. Each Basys board needs to have its own radio module connected to it. 
	Once the modules are assembled, it is time to plug the receiving board into the monitor via the VGA connection. This connection is circled in red and marked with a red “B” on Figure A. It does not matter which board you choose to be the receiver and which to be the transmitter. Each board gets its own radio module, but the receiver will be the board connected to the monitor. Figure B shown below illustrates the relationship between transmitting board and receiving board. 
	Now that radio modules are connected, and the receiving board is connected to the monitor, all that is left is a power source. The port for this on the board is circled in blue on figure A. Once you are ready to power up the board, flip the slide switch circled in orange on figure A, and the system is ready for configuration. 


III - Configuration and Usage

	Configuration of the boards is a critical step in the operation of the system. Both boards need to be configured individually in order to connect correctly. These initialization systems build the bridge between the boards, allowing for the flow of data. These systems will be gone over more thoroughly in future sections of the report, but for now, all the user needs to understand is how to trigger the initialization process. Although it sounds complicated, it is actually simple. Figure C can be found below and illustrates the button layout on each board. 


	To initialize both boards, simply power on the boards and press the center buttons. To be more precise, press the center buttons colored yellow as seen in Figure C. This must be done on both boards, pressing the center button on the receiving board does not initialize the transmitting board or vice versa. 
** Important Note **
Initialization must be performed after every power cycle

	Further operation of the board is very straight forward. On the transmitting board, the up and down buttons for controlling the game can be seen colored in with red and blue respectively. The two green buttons located on the left and right both operate as resets to the game, offering an ergonomic experience for users. While the game is being played, the running score will be displayed to the 7 Segment Display on the receiving board, circled in green in Figure A. An operational recap is offered below, showing the simplicity of the system. 

1.	Attach assembled radio modules through PMOD connections
2.	Connect VGA cable from receiver to monitor
3.	Connect power sources - power on by flipping switch 
4.	Initialize by pressing center buttons on both boards
5.	Operate the game by pressing red or blue marked buttons for up and down
6.	Resets are located on both left and right green buttons 

And with that, all of the information needed to set up and configure the system, as well as operate the game when playing, has been covered. The rest of the report will include information pertaining to the in depth operation and design of the wireless interface. 
